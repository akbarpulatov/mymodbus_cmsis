#ifndef __TIMERS_H
#define __TIMERS_H

#include "mydeff.h"

void SYSCLK_Init(void);
void Timer2_Init(void);

#endif // !__TIMERS_H
