/*
 * FreeModbus Libary: BARE Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id$
 */

#include "port.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"
#include "uart.h"

/* ----------------------- static functions ---------------------------------*/
void prvvUARTTxReadyISR( void );
void prvvUARTRxISR( void );


/* ----------------------- Start implementation -----------------------------*/
void
vMBPortSerialEnable( BOOL xRxEnable, BOOL xTxEnable )
{
    /* If xRXEnable enable serial receive interrupts. If xTxENable enable
     * transmitter empty interrupts.
     */
	
	if (xRxEnable)
	{
		//RS485 Enable Input
		DIR485 = 0;
		USART1->CR1 |= USART_CR1_RXNEIE;
	}
	else
	{
		USART1->CR1 &= ~USART_CR1_RXNEIE;
	}
  
	if (xTxEnable)
	{
		//RS485 Enable Output
        DIR485 = 1;
		USART1->CR1 |= USART_CR1_TXEIE;
	}
	else
	{
		USART1->CR1 &= ~USART_CR1_TXEIE;
	}
}

BOOL
xMBPortSerialInit( UCHAR ucPORT, ULONG ulBaudRate, UCHAR ucDataBits, eMBParity eParity )
{
	UART_Init(USART1);
    return TRUE;
}

BOOL
xMBPortSerialPutByte( CHAR ucByte )
{
    /* Put a byte in the UARTs transmit buffer. This function is called
     * by the protocol stack if pxMBFrameCBTransmitterEmpty( ) has been
     * called. */
	
	USART1->DR = ucByte;
    return TRUE;
}

BOOL xMBPortSerialGetByte( CHAR * pucByte )
{
    /* Return the byte in the UARTs receive buffer. This function is called
     * by the protocol stack after pxMBFrameCBByteReceived( ) has been called.
     */
	
	*pucByte = (uint8_t)(USART1->DR & (uint8_t)0x00FF);
    return TRUE;
}

/* Create an interrupt handler for the transmit buffer empty interrupt
 * (or an equivalent) for your target processor. This function should then
 * call pxMBFrameCBTransmitterEmpty( ) which tells the protocol stack that
 * a new character can be sent. The protocol stack will then call 
 * xMBPortSerialPutByte( ) to send the character.
 */
static uint8_t uiCnt;
void prvvUARTTxReadyISR( void )
{
	pxMBFrameCBTransmitterEmpty(  );

//	if(uiCnt++ < 10)
//	{
//		(void) xMBPortSerialPutByte('a');
//	}
//	else
//	{
//		vMBPortSerialEnable(FALSE, FALSE);
//	}
}

/* Create an interrupt handler for the receive interrupt for your target
 * processor. This function should then call pxMBFrameCBByteReceived( ). The
 * protocol stack will then call xMBPortSerialGetByte( ) to retrieve the
 * character.
 */
void prvvUARTRxISR( void )
{
    pxMBFrameCBByteReceived(  );
}

/********************************************************************************
 * Uart Interrupt Handler
 ********************************************************************************/

void USART1_IRQHandler(void)
{
	if (USART1->SR & USART_SR_RXNE)
	{
		prvvUARTRxISR(); 
		LEDG = 0;
	}
	
	else if (USART1->SR & USART_SR_TXE) 
	{
		LEDG = 1;
		prvvUARTTxReadyISR();
	}
}

/********************************* END OF FILE **********************************/
