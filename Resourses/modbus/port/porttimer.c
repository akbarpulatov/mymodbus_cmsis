/*
 * FreeModbus Libary: BARE Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id$
 */

/* ----------------------- Platform includes --------------------------------*/
#include "port.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"
#include "mydeff.h"

extern volatile DWORD msTMR1;

/* ----------------------- static functions ---------------------------------*/
static void prvvTIMERExpiredISR(void);

/* ----------------------- Start implementation -----------------------------*/
BOOL
xMBPortTimersInit(USHORT usTim1Timerout50us)
{
	msTMR1 = usTim1Timerout50us;
	return TRUE;
}


inline void
vMBPortTimersEnable()
{
	/* Enable the timer with the timeout passed to xMBPortTimersInit( ) */
	//Counter Enable/start
	TIM2->CR1 |= TIM_CR1_CEN;
	TIM2->DIER |= TIM_DIER_UIE;
}

inline void
vMBPortTimersDisable()
{
	/* Disable any pending timers. */
	//Counter Disable/stop
	TIM2->DIER &= ~TIM_DIER_UIE;
	TIM2->CR1 &= ~TIM_CR1_CEN;
}

/* Create an ISR which is called whenever the timer has expired. This function
 * must then call pxMBPortCBTimerExpired( ) to notify the protocol stack that
 * the timer has expired.
 */
static void prvvTIMERExpiredISR(void)
{
	(void)pxMBPortCBTimerExpired();
}
//------------------------------------------------------
void TIM2_IRQHandler(void)
{
	TIM2->SR &= ~TIM_SR_UIF;
	
	if (msTMR1)	msTMR1--;
	else
	{
		prvvTIMERExpiredISR();
	}

	//	if (msTMR2)	msTMR2--;
	//	if (msTMR3)	msTMR3--;
	//	if (msTMR4)	msTMR4--;
	//	if (msTMR5)	msTMR5--;
	//	if (msTMR6)	msTMR6--;
	//	if (msTMR7)	msTMR7--;
	//	if (msTMR8)	msTMR8--;
}

