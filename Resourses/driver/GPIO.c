/********************************************************************************
 * project																		*
 *																				*
 * file			GPIO.c															*
 * author		Akbar Pulatov													*
 * date																			*
 * copyright	Akbar Pulatov(C)												*
 * brief																		*
 *																				*
 ********************************************************************************/

/********************************************************************************
 * Include 
 ********************************************************************************/
#include "stm32f1xx.h"
#include "GPIO.h"

/********************************************************************************
 * Used variable
 ********************************************************************************/


/********************************************************************************
 * GPIO Initialization Function
 ********************************************************************************/

void GPIO_Init(void)
{
	RCC->APB2ENR |=  RCC_APB2ENR_IOPCEN
				 |	 RCC_APB2ENR_IOPAEN;
	
	//PC13
	GPIOC->CRH &= ~(GPIO_CRH_CNF13_Msk | GPIO_CRH_MODE13_Msk);
	GPIOC->CRH |= (0b11 << GPIO_CRH_MODE13_Pos) | (0b00 << GPIO_CRH_CNF13_Pos);
	
	//PA11
	GPIOA->CRH &= ~(GPIO_CRH_CNF11_Msk | GPIO_CRH_MODE11_Msk);
	GPIOA->CRH |= (0b11 << GPIO_CRH_MODE11_Pos) | (0b00 << GPIO_CRH_CNF11_Pos);
	
	//PC15
	GPIOC->CRH &= ~(GPIO_CRH_CNF15_Msk | GPIO_CRH_MODE15_Msk);
	GPIOC->CRH |= (0b11 << GPIO_CRH_MODE15_Pos) | (0b00 << GPIO_CRH_CNF15_Pos);
	
	//PA8 
	GPIOA->CRH &= ~(GPIO_CRH_CNF8 | GPIO_CRH_MODE8_Msk);
	GPIOA->CRH |= (0b11 << GPIO_CRH_MODE8_Pos) | (0b00 << GPIO_CRH_CNF8_Pos);
	
	
//	//PA2 for External Interrupt
//	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN
//				 |  RCC_APB2ENR_AFIOEN;
//	//PA2
//	GPIOA->CRL &= ~(GPIO_CRL_CNF2_Msk | GPIO_CRL_MODE2_Msk);
//	GPIOA->CRL |= (0b00 << GPIO_CRL_MODE2_Pos) | (0b10 << GPIO_CRL_CNF2_Pos);
//	GPIOA->ODR |= 1 << GPIO_ODR_ODR2_Pos;
//	
//	//EXTI2 for PA
//	AFIO->EXTICR[0] |= AFIO_EXTICR1_EXTI2_PA;
//	//Falling Edge
//	EXTI->FTSR |= EXTI_FTSR_TR2;
//	//Setting Up Mask
//	EXTI->IMR |= EXTI_IMR_MR2;
//	//Permit Interrupt On Line EXTI2
//	NVIC_EnableIRQ(EXTI2_IRQn);
	
}

/********************************* END OF FILE **********************************/
