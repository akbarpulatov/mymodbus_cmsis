/********************************************************************************
 * project																		*
 *																				*
 * file			uart.c															*
 * author		Akbar Pulatov													*
 * date																			*
 * copyright	Akbar Pulatov(C)												*
 * brief																		*
 *																				*
 ********************************************************************************/

/********************************************************************************
 * Include 
 ********************************************************************************/

#include "uart.h"
#include "mydeff.h"


/********************************************************************************
 * Used variable
 ********************************************************************************/


/********************************************************************************
 * Uart Initialization function
 ********************************************************************************/

void UART_Init(USART_TypeDef* UART)
{
	RCC->APB2ENR |= RCC_APB2ENR_USART1EN
				 |  RCC_APB2ENR_IOPAEN;
	
	
	//PA9 Tx AFPP
	GPIOA->CRH &= ~(GPIO_CRH_CNF9 | GPIO_CRH_MODE9_Msk);
	GPIOA->CRH |= (0b11 << GPIO_CRH_MODE9_Pos) | (0b10 << GPIO_CRH_CNF9_Pos);
	
	//PA10 Rx INF/IPUP
	GPIOA->CRH &= ~(GPIO_CRH_CNF10 | GPIO_CRH_MODE10_Msk);
	GPIOA->CRH |= (0b00 << GPIO_CRH_MODE10_Pos) | (0b10 << GPIO_CRH_CNF10_Pos);
	GPIOA->BSRR = GPIO_BSRR_BS10;
	
	
	//UART CONFIGURATION
	UART->CR1 |= USART_CR1_UE;
	UART->CR1 |= 0 << USART_CR1_M_Pos;
	UART->CR2 |= 0b00 << USART_CR2_STOP_Pos;
	//	UART->CR3 |= 1 << USART_CR3_DMAT_Pos;
	
	//	//CONFIGURE THE DMA REGISTERS
	//	RCC->AHBENR |= RCC_AHBENR_DMA1EN;
	//	
	//	DMA_UART->CCR &= ~(DMA_CCR_EN);
	//	DMA_UART->CPAR = (uint32_t)(&(UART->DR));
	//	DMA_UART->CCR |= (0b10 << DMA_CCR_PL_Pos);
	//	DMA_UART->CCR |= (1 << DMA_CCR_DIR_Pos)
	//				  |  (0 << DMA_CCR_CIRC_Pos)
	//				  |  (0 << DMA_CCR_PINC_Pos)
	//				  |  (1 << DMA_CCR_MINC_Pos)
	//				  |  (0b10 << DMA_CCR_PSIZE_Pos)
	//				  |  (0b00 << DMA_CCR_MSIZE_Pos)
	//				  |  (1 << DMA_CCR_TCIE_Pos)
	//				  |  (0 << DMA_CCR_TEIE_Pos);
	//	NVIC_EnableIRQ(DMA1_Channel4_IRQn);
	
	//BAUD RATE = 115200	
	UART->BRR &= ~(USART_BRR_DIV_Fraction | USART_BRR_DIV_Mantissa);
	UART->BRR |= (0x027 << USART_BRR_DIV_Mantissa_Pos) | (0x1 << USART_BRR_DIV_Fraction_Pos);
//	UART->BRR = 3750;
	UART->CR1 |= USART_CR1_TE | USART_CR1_RE;
	
	
//	USART1->CR1 |= USART_CR1_RXNEIE;
//	USART1->CR1 |= USART_CR1_TXEIE;
	
	NVIC_EnableIRQ(USART1_IRQn);
}



///********************************************************************************
// * Uart send function
// ********************************************************************************/
//
//void UartSend(uint8_t* pbuf, uint8_t number)
//{
//	while (number--)
//	{
//		while (true)
//		USART1->DR = *pbuf++;
//	}
//}

/********************************* END OF FILE **********************************/