/********************************************************************************
 * project																		*
 *																				*
 * file			main.c															*
 * author		Akbar Pulatov													*
 * date																			*
 * copyright	Akbar Pulatov(C)												*
 * brief																		*
 *																				*
 ********************************************************************************/

/********************************************************************************
 * Include 
 ********************************************************************************/

#include "stm32f1xx.h"
#include "Timers.h"
#include "uart.h"
#include "GPIO.h"
#include "spi.h"

#include "mb.h"
#include "mbport.h"

/********************************************************************************
 * Used variable
 ********************************************************************************/

#define REG_INPUT_START 1000
#define REG_INPUT_NREGS 10

static USHORT   usRegInputStart = REG_INPUT_START;
static USHORT   usRegInputBuf[REG_INPUT_NREGS];
uint8_t spiByte[4] = { 0xFF, 0xFF, 0xFF, 0xFF };

/********************************************************************************
 * For Modbus layer
 ********************************************************************************/

static uint32_t lock_nesting_count = 0;
void __critical_enter(void)
{
//	__disable_irq();
	++lock_nesting_count;
}
void __critical_exit(void)
{
	/* Unlock interrupts only when we are exiting the outermost nested call. */
//	--lock_nesting_count;
//	if (lock_nesting_count == 0) {
//		__enable_irq();
//	}
}

void UpdateInputs(void)
{
	Ser_ST = 0;
	Ser_ST = 1;
	for (BYTE i = 0; i < 3; i++)
	{
		spiByte[i] = SPI_ReceiveByte();
	}
}

void UpdateRegisters(void)
{
	Byte tmp0;
	Byte tmp1;
	Byte tmp2;
	Byte tmp3;
	
	tmp0._byte = ~spiByte[0];
	tmp1._byte = ~spiByte[1];
	tmp2._byte = ~spiByte[2];
	tmp3._byte = ~spiByte[3];
		
	usRegInputBuf[0] = (tmp1._byte << 8) | tmp2._byte;
	usRegInputBuf[1] = (tmp3._byte << 8) | tmp0._byte;
}

/********************************************************************************
 * main function
 ********************************************************************************/

int main(void)
{
	SYSCLK_Init();
	GPIO_Init();
	
	Timer2_Init();
	SpiInit(SPI1);
	
	eMBErrorCode    eStatus;
	
	eStatus = eMBInit(MB_RTU, MODBUSID, 0, 19200, MB_PAR_NONE);
	
	/* Enable the Modbus Protocol Stack. */
	eStatus = eMBEnable();
	
	while (1)
	{
		
		UpdateInputs();
		UpdateRegisters();
		(void)eMBPoll();
	}
}

/********************************************************************************
 * 0x04		Read AI / Read Input Registers
 ********************************************************************************/

eMBErrorCode eMBRegInputCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs)
{
//	bp;
	eMBErrorCode    eStatus = MB_ENOERR;
	int             iRegIndex;

	if ((usAddress >= REG_INPUT_START)
	    && (usAddress + usNRegs <= REG_INPUT_START + REG_INPUT_NREGS))
	{
		iRegIndex = (int)(usAddress - usRegInputStart);
		while (usNRegs > 0)
		{
			*pucRegBuffer++ =
			    (unsigned char)(usRegInputBuf[iRegIndex] >> 8);
			*pucRegBuffer++ =
			    (unsigned char)(usRegInputBuf[iRegIndex] & 0xFF);
			iRegIndex++;
			usNRegs--;
		}
	}
	else
	{
		eStatus = MB_ENOREG;
	}

	return eStatus;
}

/********************************************************************************
 * 0x03 0x06 0x10		Read AO / Read Holding Registers 
 ********************************************************************************/

eMBErrorCode eMBRegHoldingCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs, eMBRegisterMode eMode)
{
//	bp;
		if(eMode == MB_REG_READ)
	{
		eMBRegInputCB(pucRegBuffer, usAddress, usNRegs);
	}
	return MB_ENOERR;
}

/********************************************************************************
 * 0x01 0x05 0x0F	Read DO / Read Coil Status // shu komanda bilan o`qib 
 * olamiz kerakli xabar stringni 
 ********************************************************************************/

eMBErrorCode eMBRegCoilsCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNCoils, eMBRegisterMode eMode)
{
//	bp;
	return MB_ENOERR;
}

/********************************************************************************
 * 0x02 Read DI / Read Input Status
 ********************************************************************************/

eMBErrorCode eMBRegDiscreteCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNDiscrete)
{
//	bp;
	return MB_ENOERR;
}

/********************************* END OF FILE **********************************/